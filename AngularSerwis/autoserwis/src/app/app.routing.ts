import {Routes, RouterModule} from '@angular/router'

import {CarComponent} from './car/car.component';
import {PartsComponent} from './parts/parts.component';
import {RepairComponent} from './repair/repair.component';
import {MainComponent} from './main/main.component';
import {ErrorComponent} from './error/error.component';
import {Repair_PartsComponent} from './repair_parts/repair_parts.component'
import { AppComponent } from './app.component';
import {NgbdModalComponent} from './modal/modal-component';

const routes: Routes = [
 {path: '', component: MainComponent},
 {path: 'car', component: CarComponent},
 {path: 'parts', component: PartsComponent},
 {path: 'parts/repairId/:repairId', component: PartsComponent},
 {path: 'repair', component: RepairComponent},
 {path: 'repair/:carId', component: RepairComponent},
 {path: 'error', component: ErrorComponent},
 {path: 'repair_parts', component: Repair_PartsComponent},
 {path: 'repair_parts/carId/:carId', component: Repair_PartsComponent},
 {path: 'repair_parts/repairId/:repairId', component: Repair_PartsComponent},
 {path: 'repair_parts/repairId/:repairId/partId/:partId', component: Repair_PartsComponent},
 {path: 'modal', component: NgbdModalComponent},

 {path: '**', redirectTo: ''}
];

export const appRoutingModule = RouterModule.forRoot(routes); 
