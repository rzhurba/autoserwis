import { HttpClient, HttpResponse, HttpRequest, HttpHeaders } from '@angular/common/http';
import{Injectable} from '@angular/core';
import { Car } from './car/car';
import { Repair} from './repair/repair';
import {Parts} from './parts/parts';
import {Repair_Parts} from './repair_parts/repair_parts';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { map } from 'rxjs/operators';
import { Rep_Car } from './repair/rep_car';
import { CRP } from './repair_parts/rep_part_All';


@Injectable()
export class AppService{

    constructor(private _httpService: HttpClient){}

    private httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }

    getAllCars(): Observable<Car[]>{
       return this._httpService.get<Car[]>("http://localhost:59686/api/service/car");
        //.catch(this.handleError);
        //we should find how there catch errors;
    }

    getCarById(carId: string): Observable<Car>{
        return this._httpService.get<Car>("http://localhost:59686/api/service/car/"+carId);
    }

    findCar(car: Car): Observable<Car[]>{
        let body = JSON.stringify(car);
        return this._httpService.post<Car[]>("http://localhost:59686/api/service/car/find", body, this.httpOptions);
    }

    addNewCar(car: Car){
        let body = JSON.stringify(car);
        return this._httpService.post("http://localhost:59686/api/service/car/add", body, this.httpOptions);
        

    }

    updateCar(car: Car){
        let body = JSON.stringify(car);
        return this._httpService.put("http://localhost:59686/api/service/car/update", body, this.httpOptions);
    }

    deleteOneCar(carId: string){
        return this._httpService.delete("http://localhost:59686/api/service/car/delete/"+carId);
    }


    getAllRepairs(): Observable<Rep_Car[]>{
       return this._httpService.get<Rep_Car[]>("http://localhost:59686/api/service/repair");
        //.catch(this.handleError);
        //we should find how there catch errors;
    }

    getRepairById(repairId: string): Observable<Repair>{
        return this._httpService.get<Repair>("http://localhost:59686/api/service/repair/"+repairId);
    }

    getRepairsByCarId(carId: string): Observable<Rep_Car[]>{
        return this._httpService.get<Rep_Car[]>("http://localhost:59686/api/service/repair/carId/"+carId);
    }

    findRepair(repair: Repair): Observable<Repair[]>{
        let body = JSON.stringify(repair);
        return this._httpService.post<Repair[]>("http://localhost:59686/api/service/repair/find",body, this.httpOptions);
    }

    addNewRepair(repair: Repair){
        let body = JSON.stringify(repair);
        return this._httpService.post("http://localhost:59686/api/service/repair/add", body, this.httpOptions);
    }

    updateRepair(repair: Repair){
        let body = JSON.stringify(repair);
        return this._httpService.put("http://localhost:59686/api/service/repair/update", body, this.httpOptions);
    }

    deleteOneRepair(repairtId: string){
        return this._httpService.delete("http://localhost:59686/api/service/repair/delete/"+repairtId);
    }


    getAllParts(): Observable<Parts[]>{
        return this._httpService.get<Parts[]>("http://localhost:59686/api/service/parts");
         //.catch(this.handleError);
         //we should find how there catch errors;
     }

     getPartById(partId: string): Observable<Parts>{
        return this._httpService.get<Parts>("http://localhost:59686/api/service/part/"+partId);
    }

    findPart(part: Parts): Observable<Parts[]>{
        return this._httpService.post<Parts[]>("http://localhost:59686/api/service/part/find", part, this.httpOptions);
    }
 
     addNewPart(part: Parts){
         let body = JSON.stringify(part);
         return this._httpService.post("http://localhost:59686/api/service/part/add", body, this.httpOptions);
     }

     updatePart(part: Parts){
         let body = JSON.stringify(part);
         return this._httpService.put("http://localhost:59686/api/service/part/update", body, this.httpOptions);
     }
 
     deleteOnePart(partId: string){
         return this._httpService.delete("http://localhost:59686/api/service/part/delete/"+partId);
     }
 


     getAllRepair_Parts(): Observable<CRP[]>{
        return this._httpService.get<CRP[]>("http://localhost:59686/api/service/changed_parts");
         //.catch(this.handleError);
         //we should find how there catch errors;
     }

     getRepair_PartsById(r_p_Id: string): Observable<Repair_Parts>{
        return this._httpService.get<Repair_Parts>("http://localhost:59686/api/service/changed_parts/"+r_p_Id);
    }

    getRepair_PartsByRepairId(repairId: string): Observable<CRP[]>{
        return this._httpService.get<CRP[]>("http://localhost:59686/api/service/changed_parts/repairId/"+repairId);
    }

    getRepair_PartsByCarId(carId: string): Observable<CRP[]>{
        return this._httpService.get<CRP[]>("http://localhost:59686/api/service/changed_parts/carId/"+carId);
    }
 
    addNewRepair_Parts(rep_part: Repair_Parts){
        let body = JSON.stringify(rep_part);
        return this._httpService.post("http://localhost:59686/api/service/changed_parts/add", body, this.httpOptions);
         
 
    }

    updateRepair_Parts(rep_part: Repair_Parts){
        let body = JSON.stringify(rep_part);
        return this._httpService.put("http://localhost:59686/api/service/changed_parts/update",body,this.httpOptions);
    }
 
    deleteOneRepair_Parts(r_p_Id: string){
        return this._httpService.delete("http://localhost:59686/api/service/changed_parts/delete/"+r_p_Id);
    }
 

}