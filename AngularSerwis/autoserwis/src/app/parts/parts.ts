import {logging} from 'protractor';
import {Repair_Parts} from '../repair_parts/repair_parts';

export class Parts{
    partId: number;
    partBrand: String;
    partName: String;
    partNum: String;
    partPrice: number;
    rep_par: Repair_Parts[];
}