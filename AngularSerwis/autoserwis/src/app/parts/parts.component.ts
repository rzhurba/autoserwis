import {Component, OnInit, ElementRef} from '@angular/core';
import {Parts} from './parts';
import { AppService } from '../app.service';;
import { Title } from '@angular/platform-browser';

import * as moment from 'moment'
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

declare function dodajBTN_CHECK_PARTS(): any;
declare function szukajBTN_CHECK_PARTS(): any;

@Component({
    selector: 'app-parts',
    templateUrl: './parts.component.html'
})
export class PartsComponent implements OnInit{

    editMode: boolean = false;
    selectionMode: boolean = false;
    repairId: number;
    ilosc: any;
    startPoint = 0;
    finishPoint = 10; 
    parts: Parts[];
    partsToShow: Parts[];
    part = new Parts();
    constructor(private _partsService: AppService, private titleService: Title,private route: ActivatedRoute, private router: Router){
        moment.locale('fr');
        const currentTimeFR = moment().format('LLL');
    }

public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
}

onSelect() {
    this.router.navigate(['/error']);
}

goToCP() {
    if(!!this.part.partId)
    this.router.navigate(['/repair_parts/repairId', this.repairId, 'partId', this.part.partId]);
}

ngOnInit():void{
    this.setTitle("Parts")
    let repairId = parseInt(this.route.snapshot.paramMap.get('repairId'));
        if (Number.isNaN(repairId)) {
            console.log('puste');
            this.getParts();
        }else{
            this.selectionMode = true;
            this.repairId = repairId;
            this.getParts();
        }
    
    //this.getCarsToShow();
}

    getParts():void{
        this._partsService.getAllParts()
        .subscribe((partsData) => {
            this.parts = partsData,
            this.ilosc = this.parts.length,
            this.getPartsToShow(this.parts),
             console.log(partsData)
            }, (error)=>{
            console.log(error); 
        });    
    }

    getPartsToShow(parts: Parts[]):void{
        console.log('here');
        console.log('ilosc: '+this.ilosc);
        this.partsToShow = new Array<Parts>();
        if(this.ilosc>10){
            console.log('>10');
            var counter =0;
            for(var i = this.startPoint;i<this.finishPoint;i++){
                this.partsToShow[counter] = parts[i];
                counter++;
            }
        }else{
            console.log('here too');
            for(var i = this.startPoint;i<this.ilosc;i++){
                this.partsToShow[i] = parts[i];
                console.log(i+' '+parts[i]);
            }
        }
    }

    getNextPage(){
        if(this.ilosc>10){
            if(this.ilosc>(this.startPoint+10)){
                this.startPoint=this.startPoint+10;
            if(this.ilosc<(this.finishPoint+10)){
                this.finishPoint = this.ilosc;
            }else{
                this.finishPoint = this.finishPoint+10;
            }
            this.getParts();
            }
        }
    }

    getPreviousPage(){
        if(this.ilosc>10){
            if(this.finishPoint>10){
                this.startPoint = this.startPoint - 10;
                if(this.finishPoint%10==0){
                    this.finishPoint = this.finishPoint - 10;
                }else{
                    this.finishPoint = this.finishPoint -this.partsToShow.length;
                }   
                this.getParts();
            }
        }
    }

    findPart():void{
        if(szukajBTN_CHECK_PARTS()){
            //console.log(this.part)
            this._partsService.findPart(this.part)
            .subscribe((dataParts)=>{
                this.parts = dataParts,
                this.ilosc = this.parts.length,
                this.getPartsToShow(this.parts),
                console.log(dataParts)
            }, (error)=>{
            console.log(error); 
        });   
        }
    }

    getPartById(partId: string){
        this.editMode = true;
        this._partsService.getPartById(partId)
        .subscribe((partsData)=>{
            this.part = partsData; 
            //this.getParts();
        }, (error)=>{
            console.log(error);
        });
    }

    addPart(): void{
        if(dodajBTN_CHECK_PARTS()){
            this._partsService.addNewPart(this.part)
            .subscribe((response)=>{
                console.log(response);
                this.reset();
                this.getParts();
            }, (error)=>{
                console.log(error);
            });
        }
    }

    deletePart(partId: string): void{
        this.reset();
        this._partsService.deleteOnePart(partId)
        .subscribe((response)=>{
            console.log(response);
            this.getParts();
        }, (error)=>{
            console.log(error);
        });
    }

    updatePart():void{
        if(dodajBTN_CHECK_PARTS()){
            this._partsService.updatePart(this.part)
            .subscribe((response)=>{
                console.log(response);
                this.reset();
                this.getParts();
            }, (error)=>{
                console.log(error);
            });
        }
    }

    private reset(){
        this.editMode = false;
        this.part.partId = null;
        this.part.partBrand = null;
        this.part.partName = null;
        this.part.partNum = null;
        this.part.partPrice = null;
    }

} 