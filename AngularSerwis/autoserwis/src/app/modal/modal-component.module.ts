import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '../../../node_modules/@ng-bootstrap/ng-bootstrap';

import { NgbdModalComponent, NgbdModalContent } from './modal-component';
import { AppComponent } from '../app.component';

@NgModule({
  imports: [BrowserModule, NgbModule],
  declarations: [AppComponent, NgbdModalComponent, NgbdModalContent],
  exports: [NgbdModalComponent],
  bootstrap: [NgbdModalComponent],
  entryComponents: [NgbdModalContent]
})
export class NgbdModalComponentModule {}
