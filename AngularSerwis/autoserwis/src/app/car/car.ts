import {logging} from 'protractor'
import { Repair } from '../repair/repair';

export class Car{
    carId: number =null;
    carNum: String = null;
    carDate: String = null;
    carBrand: String = null;
    carModel: String = null;
    winCode: String = null;
    repairs: Repair[] = null;
    
}