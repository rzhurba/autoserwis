import {Component, OnInit, ElementRef} from '@angular/core';
import {Car} from './car';
import { AppService } from '../app.service';
import { Title } from '@angular/platform-browser';
import {Router, ActivatedRoute} from '@angular/router';

import * as moment from 'moment'
import { Observable } from 'rxjs';
import { count } from 'rxjs/operators';

declare function dodajBTN_CHECK_CAR(): any;
declare function szukajBTN_CHECK_CAR(): any;
declare function scrollDown(x): any;

@Component({
    selector: 'app-car',
    templateUrl: './car.component.html'
})
export class CarComponent implements OnInit{

    ilosc: any;
    startPoint = 0;
    finishPoint = 10; 
    cars: Car[];
    carsToShow: Car[];
    car = new Car();
    editMode: boolean = false;
    constructor(private _carService: AppService, private titleService: Title, private router: Router, private route: ActivatedRoute){
        moment.locale('fr');
        const currentTimeFR = moment().format('LLL');
    }

public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
}

ngOnInit():void{
    this.setTitle("Cars");
    this.getCars();

    //this.getCarsToShow();
}

    onRepair(Car){
        this.router.navigate(['/repair', Car.carId]);
    }

    onParts(Car){
        this.router.navigate(['/repair_parts/carId', Car.carId]);
    }

    checkMode():boolean{
        if(this.editMode){
            return true;
        }else{
            return false;
        }
    }

    getCars():void{
        this._carService.getAllCars()
        .subscribe((carData) => {
            this.cars = carData,
            this.ilosc = this.cars.length,
            this.getCarsToShow(this.cars),
             console.log(carData)
            }, (error)=>{
            console.log(error); 
        });    
    }

    getCarsToShow(cars: Car[]):void{
        console.log('here');
        console.log('ilosc: '+this.ilosc);
        this.carsToShow = new Array<Car>();
        if(this.ilosc>10){
            console.log('>10');
            var counter =0;
            for(var i = this.startPoint;i<this.finishPoint;i++){
                this.carsToShow[counter] = cars[i];
                counter++;
            }
        }else{
            console.log('here too');
            for(var i = this.startPoint;i<this.ilosc;i++){
                this.carsToShow[i] = cars[i];
                console.log(i+' '+cars[i]);
            }
        }
    }

    getNextPage(){
        if(this.ilosc>10){
            if(this.ilosc>(this.startPoint+10)){
                this.startPoint=this.startPoint+10;
            if(this.ilosc<(this.finishPoint+10)){
                this.finishPoint = this.ilosc;
            }else{
                this.finishPoint = this.finishPoint+10;
            }
            this.getCars();
            }
        }
    }

    getPreviousPage(){
        if(this.ilosc>10){
            if(this.finishPoint>10){
                this.startPoint = this.startPoint - 10;
                if(this.finishPoint%10==0){
                    this.finishPoint = this.finishPoint - 10;
                }else{
                    this.finishPoint = this.finishPoint -this.carsToShow.length;
                }   
                this.getCars();
            }
        }
    }

    getCarById(carId: string){
        this.editMode = true;
        this._carService.getCarById(carId)
        .subscribe((carData)=>{
            this.car = carData; this.getCars();
            scrollDown('btnZapisz');
        }, (error)=>{
            console.log(error);
        });
    }

    findCar():void{
        if(szukajBTN_CHECK_CAR()){
            console.log(this.car);
            this._carService.findCar(this.car)
            .subscribe((carData)=>{
                this.cars = carData,
                this.ilosc = this.cars.length,
                this.getCarsToShow(this.cars),
                 console.log(carData)
                }, (error)=>{
                console.log(error); 
            });   
        }
    }


    addCar(): void{
        if(dodajBTN_CHECK_CAR()){
            this._carService.addNewCar(this.car)
            .subscribe((response)=>{
                console.log(response);
                this.reset();
                this.getCars();
            }, (error)=>{
                console.log(error);
            });
        }
    }

    updateCar():void{
        if(dodajBTN_CHECK_CAR()){
            this._carService.updateCar(this.car)
            .subscribe((response)=>{
                console.log(response);
                this.reset();
                this.getCars();
                this.editMode = false;
            },(error)=>{
                console.log(error);
                this.editMode = false;
            });
        }
    }

    deleteCar(carId: string): void{
        this.reset();
        this.editMode = false;
        this._carService.deleteOneCar(carId)
        .subscribe((response)=>{
            console.log(response);
            this.getCars();
        }, (error)=>{
            console.log(error);
        });
    }

    private reset(){
        this.car.carId = null;
        this.car.carNum = null;
        this.car.carDate = null;
        this.car.carBrand = null;
        this.car.carModel = null;
        this.car.winCode = null;
        this.car.repairs = null;
    }

}