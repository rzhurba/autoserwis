import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import {CarComponent} from './car/car.component';
import {PartsComponent} from './parts/parts.component';
import {RepairComponent} from './repair/repair.component';
import {MainComponent} from './main/main.component';
import {ErrorComponent} from './error/error.component';
import {Repair_PartsComponent} from './repair_parts/repair_parts.component';
import { AppRoutingModule } from './app-routing.module';
import { appRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { AppService } from './app.service';
import {NgbdModalComponent} from './modal/modal-component';


@NgModule({
  declarations: [
    AppComponent, CarComponent, PartsComponent, RepairComponent, MainComponent, Repair_PartsComponent, ErrorComponent, NgbdModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, HttpClientModule, FormsModule, appRoutingModule
  ],
  providers: [AppService], 
  bootstrap: [AppComponent]
})
export class AppModule { }
