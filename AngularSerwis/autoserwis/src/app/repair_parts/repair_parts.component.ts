import { Component, OnInit, ElementRef } from '@angular/core';
import { Repair } from '../repair/repair';
import { Car } from '../car/car';
//import {Rep_Car} from './rep_car'; 
import { AppService } from '../app.service';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import {Rep_Car} from '../repair/rep_car';

import * as moment from 'moment'
import { Observable } from 'rxjs';
import { Parts } from '../parts/parts';
import { Repair_Parts } from './repair_parts';
import {CRP} from './rep_part_All';
import { ThrowStmt } from '@angular/compiler';

declare function updateBTN_CHECK_PART():any;

@Component({
    selector: 'app-repair_parts',
    templateUrl: './repair_parts.component.html'
})
export class Repair_PartsComponent implements OnInit {

    editMode: boolean = false;
    //---------------
    logo: string;
    addMode: string;
    carId: string;
    //---------------
    car: Car = new Car();
    repair: Repair = new Repair();
    part: Parts = new Parts();
    //----------------
    //modalInfoRep: NgbdModalConfig = new NgbdModalConfig();
    //----------------
    repairId: string;
    ilosc: any;
    startPoint = 0;
    finishPoint = 10; 
    crpAr: CRP[];
    crpToShowAr: CRP[];
    crpOne: CRP = new CRP();
    rep_par: Repair_Parts = new Repair_Parts();

    constructor(private _appService: AppService, private titleService: Title, private route: ActivatedRoute, private router: Router) {
        moment.locale('fr');
        const currentTimeFR = moment().format('LLL');
    }

    public setTitle(newTitle: string) {
        this.titleService.setTitle(newTitle);
    }

    onSelect(){
        this.router.navigate(['/error']);
    }
    goCar(){
        if(this.editMode)
        return;
        this.router.navigate(['/car']);
    }
    goRepair(){
        if(this.editMode)
        return;
        if(!!this.car.carId)
        this.router.navigate(['/repair/', this.car.carId]);
        else{
            alert('wybież samochód!');
        }
    }

    ngOnInit() {
        this.setTitle("Changed parts");
        let carId = parseInt(this.route.snapshot.paramMap.get('carId'));
        let repairId = parseInt(this.route.snapshot.paramMap.get('repairId'));
        let partId = parseInt(this.route.snapshot.paramMap.get('partId'));
        if(!(Number.isNaN(carId))){
            this.carId = carId.toString();
            this.logo = ' for one car';
            this.addMode = 'car';
            this.car.carId = carId;
            this.getChangedPartsForCar(carId.toString());
        } else if (!(Number.isNaN(repairId))) {
            this.repairId = repairId.toString();
            this.logo = ' for one repair';
            this.addMode = 'repair';
            this.getChangedPartsForRepair(this.repairId);
            this.getRepairById(this.repairId);
            if (!(Number.isNaN(partId))) {
                this.getPartById(partId.toString());
                this.addMode = 'add';
            }
        }else{
            this.logo = ' for all repairs:)';
            this.addMode = 'noone'
            this.getChangedPartsForAll()
        }
        console.log('carId: '+carId);
        console.log('repairId: '+repairId);
    }

    goPart(){
        if(this.editMode)
        return;
        if(!!this.repair.repairId)
        this.router.navigate(['/parts/repairId', this.repair.repairId]);
        else 
        alert('wybież samochód oraz naprawę');
    }

    getChangedPartsForAll():void{
        this._appService.getAllRepair_Parts()
        .subscribe((crpData) => {
            this.crpAr = crpData,
            this.ilosc = this.crpAr.length,
            //this.getCarsToShow(this.crpAr),
             console.log(crpData)
            }, (error)=>{
            console.log(error); 
        });    
    }

    getChangedPartsForCar(carId: string): void{
        this._appService.getRepair_PartsByCarId(carId)
        .subscribe((crpData) => {
            this.crpAr = crpData,
            this.ilosc = this.crpAr.length,
            //this.getCarsToShow(this.crpAr),
             console.log(crpData)
            }, (error)=>{
            console.log(error); 
        }); 
    }

    getChangedPartsForRepair(repairId: string):void{
        this._appService.getRepair_PartsByRepairId(repairId)
        .subscribe((crpData) => {
            this.crpAr = crpData,
            this.ilosc = this.crpAr.length,
            //this.getCarsToShow(this.crpAr),
             console.log(crpData)
            }, (error)=>{
            console.log(error); 
        }); 
    }

    getChangedPartById(c_pId: string){
        this.editMode = true;
        this._appService.getRepair_PartsById(c_pId)
        .subscribe((response)=>{
            this.rep_par = response,
            console.log(response)
        }, (error)=>{
        console.log(error); 
    }); 
    }

    updateCP() {
        if (updateBTN_CHECK_PART()) {
            if (!!this.rep_par.ilosc && this.rep_par.ilosc != 0) {//add there js checker
                console.log(this.rep_par);
                this._appService.updateRepair_Parts(this.rep_par)
                    .subscribe((response) => {
                        console.log(response);
                        this.rep_par.repairId = null;
                        this.reset();
                        this.getChangedParts();
                    }, (error) => {
                        console.log(error);
                    });
            } else {
                alert('ilość <> 0');
            }
        }
    }

    deleteChangedPart(c_pId: string){
        this._appService.deleteOneRepair_Parts(c_pId)
        .subscribe((response) => {
            console.log(response);
            this.reset();
            this.getChangedParts();
        }, (error) => {
            console.log(error);
        });
    }

    getChangedParts(){
        if(!!this.carId){
            this.getChangedPartsForCar(this.carId);
        }else if(!!this.repairId){
            this.getChangedPartsForRepair(this.repairId);
        }else{
            this.getChangedPartsForAll()
        }
    }

    getRepairById(repairId: string){
        this._appService.getRepairById(repairId)
        .subscribe((response)=>{
            this.repair = response;
            this.rep_par.repairId = response.repairId;
            this.car.carId = response.carId;
            console.log(this.repair);
        }, (error) => {
            console.log(error);
        });
    }

    getPartById(partId: string){
        //this.editMode = true;
        this._appService.getPartById(partId)
        .subscribe((partsData)=>{
            this.part = partsData; 
            this.rep_par.partId = partsData.partId;
            //this.getParts();
        }, (error)=>{
            console.log(error);
        });
    }

    addCP(){
        if(!!this.rep_par.ilosc && this.rep_par.ilosc!=0){//add there js checker
            console.log(this.rep_par);
            this._appService.addNewRepair_Parts(this.rep_par)
            .subscribe((response) => {
                console.log(response);
                this.reset();
                this.getChangedParts();
            }, (error) => {
                console.log(error);
            });
        }else{
            alert('ilość <> 0');
        }
    }

    reset(){
        this.editMode = false;
        this.rep_par.ilosc = null;
        this.rep_par.partId = null;
        //this.repair = new Repair();
        this.part = new Parts();
        //this.car = new Car();
        this.addMode = 'repair';
    }

}