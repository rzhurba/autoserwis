import {logging} from 'protractor'
import { Repair } from './repair';

export class Rep_Car{
  repair: Repair;
  carId: number;
  carNum: String;
  carModel: String;
  carBrand: String;
}