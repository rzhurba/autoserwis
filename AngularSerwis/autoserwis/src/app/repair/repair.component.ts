import { Component, OnInit, ElementRef } from '@angular/core';
import { Repair } from './repair';
import { Rep_Car } from './rep_car';
import { AppService } from '../app.service';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import * as moment from 'moment'
import { Observable } from 'rxjs';
import { Car } from '../car/car';

declare function dodajBTN_CHECK_REPAIR(): any;
declare function scrollDown(x): any;

@Component({
    selector: 'app-repair',
    templateUrl: './repair.component.html'
})
export class RepairComponent implements OnInit {

    logo: string;
    ilosc: any;
    carId: string;
    car: Car = new Car();
    editMode = false;
    startPoint = 0;
    finishPoint = 10;
    repairs: Rep_Car[];
    repairsToShow: Rep_Car[];
    repair: Rep_Car = new Rep_Car();
    repairOriginal = new Repair();

    constructor(private _appService: AppService, private titleService: Title, private route: ActivatedRoute, private router: Router) {
        moment.locale('fr');
        const currentTimeFR = moment().format('LLL');
    }

    public setTitle(newTitle: string) {
        this.titleService.setTitle(newTitle);
    }

    onSelect() {
        this.router.navigate(['/error']);
    }

    goToParts(Repair) {
        this.router.navigate(['/repair_parts/repairId', Repair.repairId]);
    }

    ngOnInit(): void {
        this.setTitle("Repair");
        let Id = parseInt(this.route.snapshot.paramMap.get('carId'));
        if (Number.isNaN(Id)) {
            console.log('puste');
            this.logo = ' for all cars';
            this.getRepairsForAll();
        } else {
            this.carId = Id.toString();
            console.log('carId: ' + this.carId)
            this.logo = ' for one car';
            this.getRepairsForOne(this.carId);
        }
        //this.getCarsToShow();
    }

    getRepairsForAll(): void {
        this._appService.getAllRepairs()
            .subscribe((repairsData) => {
                this.repairs = repairsData,
                    this.ilosc = this.repairs.length
            }, (error) => {
                console.log(error);
            });
    }

    getRepairsForOne(carId: string): void {
        this._appService.getRepairsByCarId(carId)
            .subscribe((repairsData) => {
                this.repairs = repairsData,
                this.ilosc = this.repairs.length,
                this.selectCarToAddRepair(this.repairs[0].carId.toString());
            }, (error) => {
                console.log(error);
            });
    }

    deleteRepair(repairId: string): void {
        this.reset();
        this.editMode = false;
        this._appService.deleteOneRepair(repairId)
            .subscribe((response) => {
                console.log(response);
                this.getRepairs();
            }, (error) => {
                console.log(error);
            });
    }

    getRepairs(): void {
        if (!!this.carId) {
            this.getRepairsForOne(this.carId);
        } else {
            this.getRepairsForAll();
        }
    }

    selectCarToAddRepair(carId: string) {
        this.editMode = false;
        this.reset();
        this._appService.getCarById(carId)
            .subscribe((carData) => {
                this.car = carData;
            }, (error) => {
                console.log(error);
            });
        //scrollDown("btnDodaj");
    }

    selectRepairToEdit(repair: Rep_Car){
        this.editMode = true;
        this.repairOriginal = repair.repair;
        //console.log(this.repairOriginal);
        this.car.carNum = repair.carNum;
        this.car.carBrand = repair.carBrand;
        this.car.carModel = repair.carModel;
        scrollDown("btnSzukaj");
    }

    addRepairs(): void {
        if (!(!!this.car.carId)) {
            alert('wybierz samochód');
        } else {
            if (dodajBTN_CHECK_REPAIR()) {
                this.repairOriginal.carId = this.car.carId;
                console.log(this.repairOriginal);
                this._appService.addNewRepair(this.repairOriginal)
                    .subscribe((response) => {
                        console.log(response);
                        this.reset();
                        this.getRepairs()
                    }, (error) => {
                        console.log(error);
                    });
            }
        }
    }

    updateRepairs():void{
        if(!this.repairOriginal.repairId) {
            alert('wybierz naprawę!');
        }else{
            if (dodajBTN_CHECK_REPAIR()) {
                //console.log(this.repairOriginal);
                this._appService.updateRepair(this.repairOriginal)
                    .subscribe((response) => {
                        console.log(response);
                        this.reset();
                        this.getRepairs()
                    }, (error) => {
                        console.log(error);
                    });
            }
        }
    }








    private reset() {
        this.editMode = false;
        this.repairOriginal.carId = null;
        this.repairOriginal.dateFrom = null;
        this.repairOriginal.dateTo = null;
        this.repairOriginal.mileage = null;
        this.repairOriginal.repPrice = null;
        this.repairOriginal.car = null;
        this.repairOriginal.rep_par = null;
        this.repairOriginal.repairId = null;
        this.car.carId = null;
        this.car.carNum = null;
        this.car.carDate = null;
        this.car.carBrand = null;
        this.car.carModel = null;
        this.car.winCode = null;
        this.car.repairs = null;

    }

} 