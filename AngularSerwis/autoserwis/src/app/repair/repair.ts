import {logging} from 'protractor'
import {Repair_Parts} from '../repair_parts/repair_parts';
import { Car } from '../car/car';

export class Repair{
    repairId: number;
    dateFrom: String;
    dateTo: String;
    mileage: number;
    repPrice: number;
    carId: number;
    car: Car;
    rep_par: Repair_Parts[];

}