﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace carservice_work.Models
{
    public partial class carserviceContext : DbContext
    {
        public carserviceContext()
        {
        }

        public carserviceContext(DbContextOptions<carserviceContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Car> Car { get; set; }
        public virtual DbSet<Parts> Parts { get; set; }
        public virtual DbSet<Repair> Repair { get; set; }
        public virtual DbSet<RepairParts> RepairParts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           // if (!optionsBuilder.IsConfigured)
            //{
                optionsBuilder.UseMySQL("server=localhost;port=3306;user=root;password=23510190;database=carservice");
            //}
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Car>(entity =>
            {
                entity.ToTable("car", "carservice");

                entity.Property(e => e.CarId)
                    .HasColumnName("car_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CarBrand)
                    .IsRequired()
                    .HasColumnName("car_brand")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CarDate)
                    .IsRequired()
                    .HasColumnName("car_date")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.CarModel)
                    .IsRequired()
                    .HasColumnName("car_model")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CarNum)
                    .IsRequired()
                    .HasColumnName("car_num")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.WinCode)
                    .IsRequired()
                    .HasColumnName("win_code")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Parts>(entity =>
            {
                entity.HasKey(e => e.PartId);

                entity.ToTable("parts", "carservice");

                entity.Property(e => e.PartId)
                    .HasColumnName("part_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PartBrand)
                    .IsRequired()
                    .HasColumnName("part_brand")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PartName)
                    .IsRequired()
                    .HasColumnName("part_name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PartNum)
                    .IsRequired()
                    .HasColumnName("part_num")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PartPrice)
                    .HasColumnName("part_price")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Repair>(entity =>
            {
                entity.ToTable("repair", "carservice");

                entity.HasIndex(e => e.CarId)
                    .HasName("Naprawa_Auto");

                entity.Property(e => e.RepairId)
                    .HasColumnName("repair_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CarId)
                    .HasColumnName("car_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DateFrom)
                    .IsRequired()
                    .HasColumnName("date_from")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DateTo)
                    .IsRequired()
                    .HasColumnName("date_to")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Mileage)
                    .HasColumnName("mileage")
                    .HasColumnType("int(11)");

                entity.Property(e => e.RepPrice)
                    .HasColumnName("rep_price")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Car)
                    .WithMany(p => p.Repair)
                    .HasForeignKey(d => d.CarId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Naprawa_Auto");
            });

            modelBuilder.Entity<RepairParts>(entity =>
            {
                entity.HasKey(e => new { e.RPId, e.RepairId, e.PartId });

                entity.ToTable("repair_parts", "carservice");

                entity.HasIndex(e => e.PartId)
                    .HasName("repair_parts_parts");

                entity.HasIndex(e => e.RepairId)
                    .HasName("Czesci_wymienione_Naprawa");

                entity.Property(e => e.RPId)
                    .HasColumnName("r_p_Id")
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.RepairId)
                    .HasColumnName("repair_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PartId)
                    .HasColumnName("part_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Ilosc)
                    .HasColumnName("ilosc")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Part)
                    .WithMany(p => p.RepairParts)
                    .HasForeignKey(d => d.PartId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("repair_parts_parts");

                entity.HasOne(d => d.Repair)
                    .WithMany(p => p.RepairParts)
                    .HasForeignKey(d => d.RepairId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Czesci_wymienione_Naprawa");
            });
        }
    }
}
