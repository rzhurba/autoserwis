﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace carservice_work.Models
{
    public partial class Car
    {
        public Car()
        {
            Repair = new HashSet<Repair>();
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int CarId { get; set; }
        public string CarNum { get; set; }
        public string CarDate { get; set; }
        public string CarBrand { get; set; }
        public string CarModel { get; set; }
        public string WinCode { get; set; }

        public ICollection<Repair> Repair { get; set; }
    }
}
