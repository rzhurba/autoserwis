﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace carservice_work.Models
{
    public partial class Repair
    {
        public Repair()
        {
            RepairParts = new HashSet<RepairParts>();
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int RepairId { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public int Mileage { get; set; }
        public int RepPrice { get; set; }
        public int CarId { get; set; }

        public Car Car { get; set; }
        public ICollection<RepairParts> RepairParts { get; set; }
    }
}
