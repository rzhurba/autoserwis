﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace carservice_work.Models
{
    public partial class Parts
    {
        public Parts()
        {
            RepairParts = new HashSet<RepairParts>();
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int PartId { get; set; }
        public string PartBrand { get; set; }
        public string PartName { get; set; }
        public string PartNum { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int PartPrice { get; set; }

        public ICollection<RepairParts> RepairParts { get; set; }
    }
}
