﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace carservice_work.Models
{
    public partial class RepairParts
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int RPId { get; set; }
        public int RepairId { get; set; }
        public int PartId { get; set; }
        public int Ilosc { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Parts Part { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Repair Repair { get; set; }
    }
}
