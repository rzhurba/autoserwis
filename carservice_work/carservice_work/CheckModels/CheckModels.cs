﻿using carservice_work.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace carservice_work.CheckModels
{
    public class CheckModels
    {
        public bool CheckCar(Car car)
        {
            if (car.CarNum == null || car.CarNum == "")
                return false;
            if(!(car.CarNum.Substring(0,2).All(Char.IsLetter) && car.CarNum.Substring(2,4).All(Char.IsDigit) && car.CarNum.Substring(6, 2).All(Char.IsLetter))) 
                return false;
            if (car.CarDate == null || car.CarDate == "" || car.CarDate.Length!=4 || (!car.CarDate.All(Char.IsDigit)))
                return false;
            if (car.CarBrand == null || car.CarBrand == "" || (!(car.CarBrand.All(Char.IsLetter))))
                return false;
            if (car.CarModel == null || car.CarModel == "")
                return false;
            if (car.WinCode == null || car.WinCode == "" || car.WinCode.Length != 17)
                return false;
            return true;

        }

        public bool CheckPart(Parts part)
        {
            if (part.PartBrand == null || part.PartBrand == "")
                return false;
            if (part.PartName == null || part.PartName == "" || (!part.PartName.All(Char.IsLetter)))
                return false;
            if (part.PartNum == null || part.PartNum == "")
                return false;
            if (part.PartPrice <= 0)
                return false;
            return true;
        }

        public bool CheckCP(RepairParts cp)
        {
            if (cp.PartId <= 0)
                return false;
            if (cp.RepairId <= 0)
                return false;
            if (cp.Ilosc <= 0)
                return false;
            return true;
        }

        public bool CheckRepair(Repair repair)
        {
            
            if (repair.DateFrom == null || repair.DateFrom == "" || (!repair.DateFrom.Substring(0, 2).All(Char.IsDigit) || (!repair.DateFrom.Substring(3, 2).All(Char.IsDigit)) || (!repair.DateFrom.Substring(6, 4).All(Char.IsDigit))))
                return false;
            if (repair.DateTo == null || repair.DateTo == "" || (!repair.DateTo.Substring(0, 2).All(Char.IsDigit) || (!repair.DateTo.Substring(3, 2).All(Char.IsDigit)) || (!repair.DateTo.Substring(6, 4).All(Char.IsDigit))))
                return false;
            var DateFrom = new DateTime(int.Parse(repair.DateFrom.Substring(6, 4)), int.Parse(repair.DateFrom.Substring(3, 2)), int.Parse(repair.DateFrom.Substring(0, 2)));
            var DateTo = new DateTime(int.Parse(repair.DateTo.Substring(6, 4)), int.Parse(repair.DateTo.Substring(3, 2)), int.Parse(repair.DateTo.Substring(0, 2)));
            if (DateFrom > DateTo)
                return false;
            if (repair.Mileage <= 0 )
                return false;
            if (repair.CarId <= 0)
                return false;
            return true;
        }
    }
}
