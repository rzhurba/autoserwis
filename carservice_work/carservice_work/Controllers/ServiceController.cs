﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using carservice_work.CheckModels;
using carservice_work.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace carservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCorsAttribute("*", "*", "*")]
    public class ServiceController : ControllerBase
    {
        private carserviceContext _context;
        private CheckModels cm = new CheckModels();

        public ServiceController(carserviceContext context)
        {
            this._context = context;


            // this._context = new carserviceContext();
        }



        [HttpGet("car")]
        public async Task<IActionResult> GetCar()
        {
            var result = await _context.Car.ToListAsync();

            return Ok(result);
            //return Ok(_context.Car.Skip(10).Take(10).ToList());
        }

        [HttpGet("car/{id:int}")]
        public IActionResult GetCar(int id)
        {
            var car = _context.Car.FirstOrDefault(e => e.CarId == id);
            if (car == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(car);
            }
        }

        [HttpPost("car/find")]
        public IActionResult FindCar([FromBody] Car car)
        {
            var result = _context.Car.ToList();
            if (car != null)
            {
                if(car.CarNum!=null && car.CarNum != "")
                   result = result.Where(p => p.CarNum.ToLower() == car.CarNum.ToLower()).ToList();
                if (car.CarDate != null && car.CarDate != "")
                    result = result.Where(p => p.CarDate.ToLower() == car.CarDate.ToLower()).ToList();
                if (car.CarBrand != null && car.CarBrand != "")
                   result = result.Where(p => p.CarBrand.ToLower() == car.CarBrand.ToLower()).ToList();
                if (car.CarModel != null && car.CarModel != "")
                    result = result.Where(p => p.CarModel.ToLower() == car.CarModel.ToLower()).ToList();
                if (car.WinCode != null && car.WinCode != "")
                    result = result.Where(p => p.WinCode.ToLower() == car.WinCode.ToLower()).ToList();
            }
            return Ok(result);
        }

        [HttpDelete("car/delete/{id:int}")]
        public IActionResult DeleteCar(int id)
        {
            var car = _context.Car.FirstOrDefault(e => e.CarId == id);
            if (car != null)
            {
                var _repairs = _context.Repair.Where(r => r.CarId == id).ToList();
                foreach(var repair in _repairs)
                {
                    DeleteRepair(repair.RepairId);
                }
                _context.Car.Remove(car);
                _context.SaveChanges();
                return Ok(car);
            }
            return NotFound();
        }

        [HttpPost("car/add")]
        public IActionResult AddCar([FromBody]Car car)
        {
            if (cm.CheckCar(car))
            {//check if format of car atribute is wrong!
             //return StatusCode(402,car);
                if (_context.Car.Where(p => p.CarId == car.CarId || p.WinCode == car.WinCode).Count() != 0)
                {
                    return StatusCode(401, car);
                }
                _context.Car.Add(car);
                _context.SaveChanges();
                return StatusCode(201, car);
            }
            else
            {
                return StatusCode(402, car);
            }
        }

        [HttpPut("car/update")]
        public IActionResult UpdateCar([FromBody]Car car)
        {
            if (!(cm.CheckCar(car)))//check if format of car atribute is wrong!
                return StatusCode(402, car);
            if (_context.Car.Count(e => e.CarId == car.CarId) == 0)
            {
                return NotFound();
            }
            _context.Car.Attach(car);
            _context.Entry(car).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges();

            return Ok(car);
        }

        [HttpGet("cars/page/{page:int}")]
        public IActionResult GetPageCar(int page)
        {
            var pageSize = 10;
            var total = _context.Car.Select(p => p.CarId).Count();
            var skip = pageSize * (page - 1);
            var canPage = skip < total;
            if (total > pageSize)
            {
                if (!canPage)
                {
                    return NotFound();
                }
                var cars = _context.Car.Skip(skip).Take(pageSize).ToList();
                return Ok(cars);
            }
            else
            {
                return Ok(_context.Car.ToList());
            }
        }
        //----------------------Repairs------------------Repairs------------------Repairs----------------Repairs------------Repairs----------
        //----------------------Repairs------------------Repairs------------------Repairs----------------Repairs------------Repairs----------
        //----------------------Repairs------------------Repairs------------------Repairs----------------Repairs------------Repairs----------
        [HttpGet("repair")]
        public IActionResult GetRepair()
        {
            var repairs = _context.Repair.Join(_context.Car, r => r.CarId, c => c.CarId, (r, c) => new { Repair = r, Car = c })
                .Select(p => new { p.Repair, p.Car.CarId, p.Car.CarNum, p.Car.CarModel, p.Car.CarBrand }).ToList();
            if (repairs != null)
            {
                return Ok(repairs);
            }
            else
            {
                return NotFound();
            }

            //return Ok(_context.Car.Skip(10).Take(10).ToList());
        }

        [HttpGet("repair/{id:int}")]
        public IActionResult GetRepair(int id)
        {
            var repair = _context.Repair.FirstOrDefault(e => e.RepairId == id);
            if (repair == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(repair);
            }
        }

        [HttpGet("repair/carId/{carId:int}")]
        public IActionResult GetRepairForCar(int carId)
        {
            //var repairs = _context.Repair.Where(p => p.CarId == carId).ToList();
            var repairs = _context.Repair.Join(_context.Car, r => r.CarId, c => c.CarId, (r, c) => new { Repair = r, Car = c }).Where(p => p.Repair.CarId == carId)
                .Select(p => new { p.Repair, p.Car.CarId, p.Car.CarNum, p.Car.CarModel, p.Car.CarBrand }).ToList();
            //var repairs1 = from repair in _context.Repair join car in _context.Car on repair.CarId equals car.CarId where repair.CarId == carId select new { Repair = repair, Car = car };
            //var repairs = repairs1.ToList();
            if (repairs == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(repairs);
            }
        }

        [HttpPost("repair/find")]
        public IActionResult FindRepair([FromBody] Repair repair)
        {
            var result =_context.Repair.Join(_context.Car, r => r.CarId, c => c.CarId, (r, c) => new { Repair = r, Car = c })
                .Select(p => new { p.Repair, p.Car.CarId, p.Car.CarNum, p.Car.CarModel, p.Car.CarBrand }).ToList();

            if (repair != null)
            {
                if (repair.DateFrom != null && repair.DateFrom != "")
                    result = result.Where(p => p.Repair.DateFrom.ToLower() == repair.DateFrom.ToLower()).ToList();
                if (repair.DateTo != null && repair.DateTo != "")
                    result = result.Where(p => p.Repair.DateTo.ToLower() == repair.DateTo.ToLower()).ToList();
                if (repair.Mileage!=0)
                    result = result.Where(p => p.Repair.Mileage == repair.Mileage).ToList();
                if (repair.RepPrice != 0)
                    result = result.Where(p => p.Repair.RepPrice == repair.RepPrice).ToList();
            }
            return Ok(result);
        }

        [HttpDelete("repair/delete/{id:int}")]
        public void DeleteRepair(int id)
        {
            var repair = _context.Repair.FirstOrDefault(e => e.RepairId == id);
            var _changed_parts = _context.RepairParts.Where(p => p.RepairId == id).ToList();
            foreach(var changed_part in _changed_parts)
            {
                DeleteChanged_part(changed_part.RPId);
            }
            _context.SaveChanges();
            if (repair != null)
            {
                _context.Repair.Remove(repair);
                _context.SaveChanges();
            }
        }

        [HttpPost("repair/add")]
        public IActionResult AddRepair([FromBody]Repair repair)
        {
            if (!(cm.CheckRepair(repair)))//check if format of repair atribute is wrong!
                return StatusCode(401, repair);
            if (_context.Repair.Where(p=>p.RepairId == repair.RepairId).Count() != 0)
            {
                return StatusCode(401, repair);
            }
            _context.Repair.Add(repair);
            _context.SaveChanges();
            return StatusCode(201, repair);
        }

        [HttpPut("repair/update")]
        public IActionResult UpdateRepair([FromBody]Repair repair)
        {
            if (!(cm.CheckRepair(repair)))//check if format of repair atribute is wrong!
                return StatusCode(401, repair);
            if (_context.Repair.Count(e => e.RepairId == repair.RepairId) == 0)
            {
                return NotFound();
            }
            _context.Repair.Attach(repair);
            _context.Entry(repair).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges();

            return Ok(repair);
        }



        [HttpGet("repair/page/{page:int}")]
        public IActionResult GetPageRepair(int page)
        {
            var pageSize = 10;
            var total = _context.Repair.Select(p => p.RepairId).Count();
            var skip = pageSize * (page - 1);
            var canPage = skip < total;
            if (total > pageSize)
            {
                if (!canPage)
                {
                    return NotFound();
                }
                var repair = _context.Repair.Skip(skip).Take(pageSize).ToList();
                return Ok(repair);
            }
            else
            {
                return Ok(_context.Repair.ToList());
            }
        }
        //-------------------Parts---------------------Parts-----------------Parts----------------Parts--------------Parts--------------Parts------
        //-------------------Parts---------------------Parts-----------------Parts----------------Parts--------------Parts--------------Parts------
        //-------------------Parts---------------------Parts-----------------Parts----------------Parts--------------Parts--------------Parts------

        [HttpGet("parts")]
        public IActionResult GetParts()
        {
            return Ok(_context.Parts.ToList());
            //return Ok(_context.Car.Skip(10).Take(10).ToList());
        }

        [HttpGet("part/{id:int}")]
        public IActionResult GetPart(int id)
        {
            var part = _context.Parts.FirstOrDefault(e => e.PartId == id);
            if (part == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(part);
            }
        }

        [HttpPost("part/find")]
        public IActionResult PartRepair([FromBody] Parts part)
        {
            var result = _context.Parts.ToList();

            if (part != null)
            {
                if (part.PartBrand != null && part.PartBrand != "")
                    result = result.Where(p => p.PartBrand.ToLower() == part.PartBrand.ToLower()).ToList();
                if (part.PartName != null && part.PartName != "")
                    result = result.Where(p => p.PartName.ToLower() == part.PartName.ToLower()).ToList();
                if (part.PartNum != null && part.PartNum != "")
                    result = result.Where(p => p.PartNum.ToLower() == part.PartNum.ToLower()).ToList();
                if (part.PartPrice != 0)
                    result = result.Where(p => p.PartPrice == part.PartPrice).ToList();
            }
            return Ok(result);
        }

        [HttpDelete("part/delete/{id:int}")]
        public IActionResult DeletePart(int id)
        {
            var part = _context.Parts.FirstOrDefault(e => e.PartId == id);
            var _changed_parts = _context.RepairParts.Where(p => p.PartId == id).ToList();
            foreach (var changed_part in _changed_parts)
            {
                DeleteChanged_part(changed_part.RPId);
            }
            _context.SaveChanges();
            if (part != null)
            {
                _context.Parts.Remove(part);
                _context.SaveChanges();
                return Ok(part);
            }
            return NotFound();
        }

        [HttpPost("part/add")]
        public IActionResult AddPart([FromBody]Parts part)
        {
            if (!(cm.CheckPart(part)))//check if format of part atribute is wrong!
                return StatusCode(402, part);
            if (_context.Parts.Where(p=>p.PartId==part.PartId || p.PartNum == part.PartNum).Count() != 0)
            {
                return StatusCode(401, part);
            }
            _context.Parts.Add(part);
            _context.SaveChanges();
            return StatusCode(201, part);
        }

        [HttpPut("part/update")]
        public IActionResult UpdatePart([FromBody]Parts part)
        {
            if (!(cm.CheckPart(part)))//check if format of part atribute is wrong!
                return StatusCode(402, part);
            if (_context.Parts.Count(e => e.PartId == part.PartId) == 0)
            {
                return NotFound();
            }
            _context.Parts.Attach(part);
            _context.Entry(part).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges();

            return Ok(part);
        }

        [HttpGet("part/page/{page:int}")]
        public IActionResult GetPageParts(int page)
        {
            var pageSize = 10;
            var total = _context.Parts.Select(p => p.PartId).Count();
            var skip = pageSize * (page - 1);
            var canPage = skip < total;
            if (total > pageSize)
            {
                if (!canPage)
                {
                    return NotFound();
                }
                var part = _context.Parts.Skip(skip).Take(pageSize).ToList();
                return Ok(part);
            }
            else
            {
                return Ok(_context.Parts.ToList());
            }
        }
        //-------------REPAIR_PARTS------------------REPAIR_PARTS---------------------REPAIR_PARTS--------------REPAIR_PARTS------------------------
        //-------------REPAIR_PARTS------------------REPAIR_PARTS---------------------REPAIR_PARTS--------------REPAIR_PARTS------------------------
        //-------------REPAIR_PARTS------------------REPAIR_PARTS---------------------REPAIR_PARTS--------------REPAIR_PARTS------------------------

        [HttpGet("changed_parts")]
        public IActionResult GetChanged_parts()
        {
            var changed_parts = (from r in _context.Repair
                                 join r_p in _context.RepairParts on r.RepairId equals r_p.RepairId
                                 join p in _context.Parts on r_p.PartId equals p.PartId
                                 select new { Repair = r, RepairParts = r_p, Parts = p }).Select(p => new
                                 {//pożniej jeszcze id r_p bedzie 
                                     p.Repair.RepairId,
                                     p.Parts,
                                     p.RepairParts.Ilosc,
                                     p.RepairParts.RPId
                                 }).ToList();
            if (changed_parts != null)
                return Ok(changed_parts);
            return NotFound();
            //return Ok(_context.Car.Skip(10).Take(10).ToList());
        }

        [HttpGet("changed_parts/{repair_partId:int}")]
        public IActionResult GetChanged_part(int repair_partId)
        {
            var changed_parts = _context.RepairParts.FirstOrDefault(p => p.RPId == repair_partId);
            if (changed_parts != null)
                return Ok(changed_parts);
            return NotFound();
        }

        [HttpGet("changed_parts/repairId/{repairId:int}")]
        public IActionResult GetChanged_partToRepair(int repairId)
        {
            var changed_parts = (from r in _context.Repair
                                 join r_p in _context.RepairParts on r.RepairId equals r_p.RepairId
                                 join p in _context.Parts on r_p.PartId equals p.PartId
                                 select new { Repair = r, RepairParts = r_p, Parts = p })
                                 .Where(p => p.Repair.RepairId == repairId)
                                 .Select(p => new
                                 { 
                                     p.Repair.RepairId,
                                     p.Parts,
                                     p.RepairParts.Ilosc,
                                     p.RepairParts.RPId
                                 }).ToList();
            if (changed_parts != null)
                return Ok(changed_parts);
            return NotFound();
        }

        [HttpGet("changed_parts/carId/{carId:int}")]
        public IActionResult GetChanged_partToCar(int carId)
        {
            var changed_parts = (from r in _context.Repair
                                 join r_p in _context.RepairParts on r.RepairId equals r_p.RepairId
                                 join p in _context.Parts on r_p.PartId equals p.PartId
                                 select new { Repair = r, RepairParts = r_p, Parts = p })
                                 .Where(p => p.Repair.CarId == carId)
                                 .Select(p => new
                                 {
                                     p.Repair.RepairId,
                                     p.Parts,
                                     p.RepairParts.Ilosc,
                                     p.RepairParts.RPId
                                 }).ToList();
            if (changed_parts != null)
                return Ok(changed_parts);
            return NotFound();
        }

        [HttpDelete("changed_parts/delete/{id:int}")]
        public IActionResult DeleteChanged_part(int id)
        {
            var changed_part = _context.RepairParts.FirstOrDefault(p => p.RPId == id);
            if(changed_part == null)
            {
                return NotFound();
            }
            else
            {
                _context.RepairParts.Remove(changed_part);
                _context.SaveChanges();
                return Ok(changed_part);
            }
        }

        [HttpPost("changed_parts/add")]
        public IActionResult AddChanged_part([FromBody]RepairParts rep_par)
        {
            if (!(cm.CheckCP(rep_par)))//check if format of part atribute is wrong!
                return StatusCode(402, rep_par);
            if (_context.RepairParts.Where(p=>p.RPId == rep_par.RPId || (p.PartId == rep_par.PartId && p.RepairId == rep_par.RepairId)).Count() != 0)
            {
                return StatusCode(401, rep_par);
            }
            else
            {
                _context.RepairParts.Add(rep_par);
                _context.SaveChanges();
                return StatusCode(201, rep_par);
            }
        }

        [HttpPut("changed_parts/update")]
        public IActionResult UpdateChanged_part([FromBody]RepairParts cpart)
        {
            if (!(cm.CheckCP(cpart)))//check if format of part atribute is wrong!
                return StatusCode(402, cpart);
            if (_context.RepairParts.Count(e => e.RPId == cpart.RPId) == 0)
            {
                return NotFound();
            }
            _context.RepairParts.Attach(cpart);
            _context.Entry(cpart).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges();

            return Ok(cpart);
        }

        [HttpGet("changed_parts/page/{page:int}")]
        public IActionResult GetPageChanged_parts(int page)
        {
            var pageSize = 10;
            var total = _context.RepairParts.Select(p => p.Ilosc).Count();
            var skip = pageSize * (page - 1);
            var canPage = skip < total;
            if (total > pageSize)
            {
                if (!canPage)
                {
                    return NotFound();
                }
                var changed_parts = _context.RepairParts.Skip(skip).Take(pageSize).ToList();
                return Ok(changed_parts);
            }
            else
            {
                return Ok(_context.RepairParts.ToList());
            }
        }
    }
}